package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.smochalkin.tm.api.service.ISessionService;
import ru.smochalkin.tm.api.service.IUserService;
import ru.smochalkin.tm.configuration.ContextConfiguration;
import ru.smochalkin.tm.dto.SessionDto;
import ru.smochalkin.tm.dto.UserDto;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.system.AccessDeniedException;

import java.util.List;

public class SessionServiceTest {

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private IUserService userService;

    private int sessionCount;

    @Before
    public void init() {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ContextConfiguration.class);
        sessionService = context.getBean(ISessionService.class);
        userService = context.getBean(IUserService.class);
        userService.create("test1", "1","@");
        userService.create("test2", "2","@");
        sessionService.open("test1","1");
        sessionCount = sessionService.getCount();
    }

    @After
    public void end() {
        @NotNull final UserDto userDto1 = userService.findByLogin("test1");
        @NotNull final UserDto userDto2 = userService.findByLogin("test2");
        sessionService.closeAllByUserId(userDto1.getId());
        sessionService.closeAllByUserId(userDto2.getId());
        userService.removeByLogin("test1");
        userService.removeByLogin("test2");
    }

    @Test
    public void openTest() {
        sessionService.open("test2", "2");
        Assert.assertEquals(sessionCount + 1, sessionService.getCount());
    }

    @Test
    public void closeTest() {
        @NotNull final UserDto userDto = userService.findByLogin("test1");
        @NotNull final SessionDto sessionDto = sessionService.findAllByUserId(userDto.getId()).get(0);
        sessionService.close(sessionDto);
        Assert.assertEquals(sessionCount - 1, sessionService.getCount());
    }

    @Test
    public void closeAllByUserIdTest() {
        @NotNull final String userId = userService.findByLogin("test1").getId();
        @NotNull final List<SessionDto> sessionDtos = sessionService.findAllByUserId(userId);
        @NotNull final int resultSize = sessionService.getCount() - sessionDtos.size();
        sessionService.closeAllByUserId(userId);
        Assert.assertEquals(resultSize, sessionService.getCount());
    }

    @Test
    public void validateTest() {
        @NotNull final SessionDto sessionDto = sessionService.open("test1", "1");
        sessionDto.setSignature(null);
        Assert.assertThrows(AccessDeniedException.class,
                () -> sessionService.validate(sessionDto));
    }

    @Test
    public void validateRoleTest() {
        @NotNull final SessionDto sessionDto = sessionService.open("test1", "1");
        Assert.assertThrows(AccessDeniedException.class,
                () -> sessionService.validate(sessionDto, Role.ADMIN));
    }

    @Test
    public void findAllByUserIdTest() {
        @NotNull final String userId = sessionService.findAll().get(0).getUserId();
        @NotNull final List<SessionDto> sessionDtos = sessionService.findAllByUserId(userId);
        Assert.assertEquals(userId, sessionDtos.get(0).getUserId());
    }

}
