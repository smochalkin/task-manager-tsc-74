package ru.smochalkin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.IBusinessRepository;
import ru.smochalkin.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    void bindTaskById(@NotNull String projectId, @NotNull  String taskId);

    void unbindTaskById(@NotNull String taskId);

    @NotNull
    List<Task> findTasksByProjectId(@NotNull String projectId);

    void removeTasksByProjectId(@NotNull String projectId);

}
