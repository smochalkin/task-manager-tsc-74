package ru.smochalkin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractTaskListener;
import ru.smochalkin.tm.endpoint.TaskDto;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public final class TaskShowByIdListener extends AbstractTaskListener {

    @Override
    @NotNull
    public String name() {
        return "task-show-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Show task by id.";
    }

    @Override
    @EventListener(condition = "@taskShowByIdListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter id: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final TaskDto task = taskEndpoint.findTaskById(sessionService.getSession(), id);
        showTask(task);
    }

}
