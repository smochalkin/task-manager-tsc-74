package ru.smochalkin.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    List<Task> findAll(@NotNull String userId);

    void addAll(Collection<Task> collection);

    @SneakyThrows
    void addAll(String userId, @Nullable Collection<Task> collection);

    Task save(Task entity);

    @Nullable
    @SneakyThrows
    Task save(String userId, @Nullable Task entity);

    void create(String userId);

    void create();

    Task findById(String id);

    Task findById(@NotNull String userId, @Nullable String id);

    void clear();

    @SneakyThrows
    void clear(@NotNull String userId);

    void removeById(String id);

    @SneakyThrows
    void removeById(@NotNull String userId, @Nullable String id);

    void remove(Task entity);

}
