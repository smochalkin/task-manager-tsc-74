package ru.smochalkin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import ru.smochalkin.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public class AbstractBusinessEntity {

    @Id
    protected String id = UUID.randomUUID().toString();

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date created = new Date();

    @Column(name = "end_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date endDate;

    @Column(name = "start_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date startDate;

    protected String description = "";

    protected String name = "";

    @Enumerated(EnumType.STRING)
    protected Status status = Status.NOT_STARTED;

    @Column(name = "user_id")
    private String userId;

}