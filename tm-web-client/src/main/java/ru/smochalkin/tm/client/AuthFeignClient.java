package ru.smochalkin.tm.client;

import feign.Feign;
import feign.okhttp.OkHttpClient;
import okhttp3.JavaNetCookieJar;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.smochalkin.tm.model.Result;

import java.net.CookieManager;
import java.net.CookiePolicy;

public interface AuthFeignClient {

    String URL = "http://localhost:8080/auth/";

    static AuthFeignClient client() {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        final CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        final okhttp3.OkHttpClient.Builder builder = new okhttp3.OkHttpClient().newBuilder();
        builder.cookieJar(new JavaNetCookieJar(cookieManager));
        return Feign.builder()
                .client(new OkHttpClient(builder.build()))
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(AuthFeignClient.class, URL);
    }

    @GetMapping(value = "/login",
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = "Accept=application/json")
    Result login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password
    );

    @GetMapping(value = "/logout",
            produces = MediaType.APPLICATION_JSON_VALUE,
            headers = "Accept=application/json")
    Result logout();

}
