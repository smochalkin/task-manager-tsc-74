package ru.smochalkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.smochalkin.tm.configuration.ApplicationConfiguration;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.util.UserUtil;

import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class TaskRepositoryTest {

    @Nullable
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private Task task;

    @NotNull
    private static String USER_ID;

    @Before
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext()
                .setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        taskRepository.deleteAll();
        @NotNull final Task task = new Task("Task");
        task.setUserId(USER_ID);
        this.task = taskRepository.save(task);
    }

    @Test
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("Task", task.getName());

        @Nullable final Task taskById = taskRepository.findById(task.getId()).orElse(null);
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task.getId(), taskById.getId());
    }

    @Test
    public void findAll() {
        @NotNull final List<Task> tasks = taskRepository.findAll();
        Assert.assertTrue(tasks.size() > 0);
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Task> tasks = taskRepository.findAllByUserId(USER_ID);
        Assert.assertTrue(tasks.size() > 0);
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Task> tasks = taskRepository.findAllByUserId("test");
        Assert.assertNotEquals(1, tasks.size());
    }

    @Test
    public void findById() {
        @Nullable final Task task = taskRepository.findByUserIdAndId(USER_ID, this.task.getId());
        Assert.assertNotNull(task);
    }

    @Test
    public void findByIdIncorrect() {
        @NotNull final Task task = taskRepository.findByUserIdAndId(USER_ID, "00");
        Assert.assertNull(task);
    }

    @Test
    public void findByIdIncorrectUser() {
        @Nullable final Task task = taskRepository.findByUserIdAndId("test", this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    public void remove() {
        taskRepository.deleteById(task.getId());
        Assert.assertFalse(taskRepository.findById(task.getId()).isPresent());
    }

    @Test
    public void findByName() {
        @NotNull final Task task = taskRepository.findByUserIdAndName(USER_ID, "Task");
        Assert.assertNotNull(task);
    }

    @Test
    public void findByNameIncorrect() {
        @NotNull final Task task = taskRepository.findByUserIdAndName(USER_ID, "00");
        Assert.assertNull(task);
    }

    @Test
    public void findByNameIncorrectUser() {
        @NotNull final Task task = taskRepository.findByUserIdAndName("test", this.task.getName());
        Assert.assertNull(task);
    }

}