package ru.smochalkin.tm.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.smochalkin.tm.api.service.ITaskService;
import ru.smochalkin.tm.configuration.ApplicationConfiguration;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.util.UserUtil;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = {ApplicationConfiguration.class })
public class TaskControllerTest {

    private final Task task = new Task("test");

    @NotNull
    @Autowired
    private ITaskService service;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Nullable
    private String USER_ID;

    @Before
    public void auth() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        service.clear();
    }

    @Test
    @SneakyThrows
    public void create() {
        @NotNull String url = "/task/create";
        this.mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @NotNull final List<Task> list = service.findAll();
        Assert.assertEquals(1, service.findAll().size());
    }

    @Test
    @SneakyThrows
    public void delete() {
        service.save(USER_ID, task);
        Assert.assertEquals(1, service.findAll().size());
        @NotNull final String url = "/task/delete/" + task.getId();
        this.mockMvc.perform(MockMvcRequestBuilders.get(url)).andDo(print())
                .andExpect(status().is3xxRedirection());
        service.findById(task.getId());
        Assert.assertEquals(0, service.findAll().size());
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final String url = "/tasks";
        this.mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
